﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadController : MonoBehaviour {

    public GameObject[] roadPrefabs;
    public GameObject redCubePrefab;
    public GameObject blueCubePrefab;
    public PlayerHealth _playerHealth;
    public float spacing = 2f;
 //   public LayerMask layer;
    

    private Transform playerTransform;
    private int amnRoadsOnScreen = 1;
    private float roadLength = 50.0f;
    private float spawnZ = 50.0f;
   // private float safeZone = 25.0f;                         //use only with DeleteRoad()
    private float randX;
    private float randX_2;
    private float randZ;
    private float randZ_2;
    private Vector3 bluePosition;
    private Vector3 redPosition;
    private List<GameObject> activeRoads;


    // Use this for initialization
    void Start()
    {
        activeRoads = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        for (int i = 0; i < amnRoadsOnScreen; i++)  //used for initialization multiple road type
        {
            SpawnRoad();
            MakeBlueObject(20, 15);
            MakeRedObject(25, 25);
        }

    }

    // Update is called once per frame
    void Update()
    {                                   //safeZone is used only if need to delete road
        
        if (playerTransform.position.z /*- safeZone*/  > (spawnZ - amnRoadsOnScreen * roadLength) - 25)  //define trigger pozition
        {
            SpawnRoad();
         //   DeleteRoad();
            MakeBlueObject(0, 0);
            MakeRedObject(0, 0);
            MakeBlueObject(10, 15);
            MakeRedObject(10, 25);
        }

    }

    private void SpawnRoad(int prefabIndex = -1)
    {
        GameObject go;
        go = Instantiate(roadPrefabs[0]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        spawnZ += roadLength;
        activeRoads.Add(go);

    }

    //private void DeleteRoad() 
    //{
    //    Destroy(activeRoads[0]);
    //    activeRoads.RemoveAt(0);
    //}

    private void MakeBlueObject(int min, int max)
    {
        randX = Random.Range(-7f, 7f);
        randZ = Random.Range(15f + min, 35f + max);
        bluePosition = new Vector3(randX, 1.0f, playerTransform.position.z + randZ);
        Instantiate(blueCubePrefab, bluePosition, Quaternion.identity);
    }

    private void MakeRedObject(int min, int max)
    {
        randX_2 = Random.Range(-7f, 7f);
        randZ_2 = Random.Range(10f + min, 35f + max);
        redPosition = new Vector3(randX_2, 1.0f, playerTransform.position.z + randZ_2);
        Instantiate(redCubePrefab, redPosition, Quaternion.identity);
    }

    //void OnCollisionExit(Collision collision) 
    //{
    //    if ((collision.gameObject.name == "Ground") || (collision.gameObject.name == "Ground(Clone)"))
    //    {
    //        Debug.Log("Exit road collide");
    //        _playerHealth.Death();
    //    }
    //}

    

}
