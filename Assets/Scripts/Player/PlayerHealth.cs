﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public PlayerController _playerController;
    public int startingHealth = 100;
    public int currentHealth;
    public float bonusSpeed = 9.9f;
    [HideInInspector] public Slider healthSlider;
    [HideInInspector] public bool isDead;
   

    private int myScene;
    private int redColorLimit = 30;


    // Use this for initialization
    void Start () {
        _playerController = GetComponent<PlayerController>();
        myScene = SceneManager.GetActiveScene().buildIndex;      //get index from current scene
        currentHealth = startingHealth;                          //start with full life
        healthSlider.value = currentHealth;                      //update slider
        InvokeRepeating("SpeedCheck", 1.0f, 1.0f);              //call function every second
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LateUpdate()
    {
        if(currentHealth < redColorLimit)
        {
            _playerController.ChangeColor(true);
        }
        else
        {
            _playerController.ChangeColor(false);
        }
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        healthSlider.value = currentHealth;

        if(currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    public void TakeBonus(int amount)
    {
        if (currentHealth + amount > 100)
        {
            currentHealth += (startingHealth - currentHealth);
        }
        else
        {
            currentHealth += amount;
        }
       
        healthSlider.value = currentHealth;
    }

    public void SpeedCheck()
    {
        float speedProcent = (float)70/100 * _playerController.maxSpeed;
        Debug.Log("tick  "+ _playerController.currentSpeed);

        if(_playerController.currentSpeed < speedProcent)
        {
            Debug.Log("Speed penality 1%");
            TakeDamage(1);     //reduce 1% of life
        }

        if (_playerController.currentSpeed > bonusSpeed && currentHealth < startingHealth)
        {
            Debug.Log("Speed bonus 1%");
            TakeBonus(1);     //bonus 1% of life
        }

    }
    public void Death()
    {
        Debug.Log("dead");
        isDead = true;
        Reload();
    }

    public void Reload()
    {
       SceneManager.LoadScene(myScene);
    }

}
