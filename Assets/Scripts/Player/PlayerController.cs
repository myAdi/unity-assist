﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public PlayerHealth _playerHealth;
    public float acceleration;
    public float maxSpeed = 10f;
    public float currentSpeed;
    public Slider SpeedSplider;
    public Material[] material;
    public Vector3 currentPosition;

    private Rigidbody rb;
    private Renderer rend;
  //  private float distToGround = 0.5f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();      //used for color change
        rend.enabled = true;
        rend.sharedMaterial = material[0];
    }

    void FixedUpdate()
    {   //get input from keybord
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //define movement
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        //addforce in movement direction
        rb.AddForce(movement * acceleration);

        //update pozition
        currentPosition = rb.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
        }

        currentSpeed = rb.velocity.z;
        SpeedSplider.value = currentSpeed;

        if (IsOnRoad() != true)          
            _playerHealth.Death();       
    }

    public void ChangeColor(bool val)
    {
        if (val)
            rend.sharedMaterial = material[1];
        else
            rend.sharedMaterial = material[0];
    }

    private bool IsOnRoad()
    {

        if ((rb.transform.position.x > -7f) && (rb.transform.position.x < 7f))
        {
            return true;
        }
        return false;

        // return Physics.Raycast(transform.position, Vector3.down, distToGround);

        //RaycastHit hit;

        //if (Physics.Raycast(transform.position, -Vector3.up, out hit, distToGround))
        //    return true;
        //return false;
    
    }

    /*
    void OnGUI() //*********** for debug only
    {   
        
        GUI.Label(new Rect(650, 20, 200, 200), "rigidbody velocity: " + currentSpeed);
    }
    */
}
