﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour {

    public PlayerController _playerController;
    public PlayerHealth _playerHealth;
    public GameObject blueParticle;
    public int blueBonus = 10;
    
    // Use this for initialization
    void Start () {
        blueParticle.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.gameObject.name == "BlueCube") || (collision.gameObject.name == "BlueCube(Clone)"))
        {
            Destroy(collision.gameObject);
            GetEmission();
            Debug.Log("Coliziune cub albastru");
           _playerHealth.TakeBonus(blueBonus);
        }
    }

    private void GetEmission()
    {
        blueParticle.transform.position = _playerController.currentPosition;
        blueParticle.SetActive(true);
        StartCoroutine("ParticleRestart");
    }

    private IEnumerator ParticleRestart()
    {
        yield return new WaitForSeconds(2);
        blueParticle.SetActive(false);
    }
}
