﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

    public PlayerHealth _playerHealth;
    public GameObject redParticle;
    public PlayerController _playerController;

    // Use this for initialization
    void Start () {
        redParticle.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.gameObject.name == "RedCube") || (collision.gameObject.name == "RedCube(Clone)"))
        {
            Debug.Log("Coliziune cub rosu");
            Destroy(collision.gameObject);
            GetEmission();
            
           
        }
    }

    private void GetEmission()
    {
        redParticle.transform.position = _playerController.currentPosition;
        redParticle.SetActive(true);
        StartCoroutine("ParticleRestart");
    }

    private IEnumerator ParticleRestart()
    {
        yield return new WaitForSeconds(1);
        redParticle.SetActive(false);
        _playerHealth.Death();
    }
}
