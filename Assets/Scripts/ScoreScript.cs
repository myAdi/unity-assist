﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

   
    public Text m_MyText;
    public static int score;
    public static int highscore;


    // Use this for initialization
    void Start () {
       // DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        
        m_MyText.text = "Score : " + GetScore() /*+ "  HighScore: "+ GetHighScore()*/;
    }

    private int GetScore()
    {
       return score = (int)Time.timeSinceLevelLoad;
    }

    private int GetHighScore()
    {
        if (highscore < score)
            highscore = score;
        return highscore;
    }
}
